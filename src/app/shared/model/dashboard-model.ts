export interface ActivityReport {
    userName: string;
    profileViewedCount: number;
    jobSearchedCount: number;
    messageSentCount: number;
    profileConnectCount: number;
    globalSearchedCount: number;
    peopleSearchedCount: number;
    avgTimeSpent: number;
    totalTimeSpent: number;
    sessionError: string;
}

export interface Domain {
    id: number;
    name: string;
}

export interface ActivityTime {
    approxTime: string;
}

export interface DomainColumns {
    columnName: string;
}