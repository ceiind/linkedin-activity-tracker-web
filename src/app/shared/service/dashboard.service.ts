import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivityReport, Domain, ActivityTime, DomainColumns } from '../model/dashboard-model';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    private apiServer = environment.activityTrackerRest;

    constructor(private httpClient: HttpClient) { }

    getAllDomains(): Observable<Domain[]> {
        return this.httpClient.get<Domain[]>(this.apiServer + '/activity/domain');
    }

    getTodayReport(id: any, date: any): Observable<ActivityReport[]> {
        return this.httpClient.get<ActivityReport[]>(this.apiServer + '/activity/report/' + id + '/' + date);
    }

    getActivityReport(id: any, fromDate: any, toDate: any): Observable<ActivityReport[]> {
        return this.httpClient.get<ActivityReport[]>(this.apiServer + '/activity/report/' + id + '/' + fromDate + '/' + toDate);
    }

    getActivityTime(id: any): Observable<ActivityTime[]> {
        return this.httpClient.get<ActivityTime[]>(this.apiServer + '/activity/time/' + id);
    }

    getDomainColumns(id: any): Observable<DomainColumns[]> {
        return this.httpClient.get<DomainColumns[]>(this.apiServer + '/activity/column/' + id);
    }

}
