import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardService } from './shared/service/dashboard.service';


const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import("../dashboard/dashboard.module").then(m => m.DashboardModule)
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [DashboardService]
})
export class AppRoutingModule { }
