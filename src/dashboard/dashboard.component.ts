import { Component, OnInit } from '@angular/core';
import { ActivityReport } from '../app/shared/model/dashboard-model';
import { DashboardService } from '../app/shared/service/dashboard.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  reportList: ActivityReport[] = [];
  defaultDomainId = 1;
  domainList: any;
  activityTimeList: any;
  columnList: any;
  readonly DELIMITER = '-';
  hoveredDate: any | null = null;
  fromDate: any;
  toDate: any | null = null;
  page = 1;
  pageSize = 10;

  constructor(private dashboardService: DashboardService, private calendar: NgbCalendar, private spinnerService: NgxSpinnerService, public formatter: NgbDateParserFormatter) {
    this.fromDate = calendar.getToday();
    this.toDate = null;
  }

  dashboardForm = new FormGroup({
    domain: new FormControl(1, Validators.required)
  });

  get formControls() {
    return this.dashboardForm.controls;
  }

  ngOnInit(): void {
    this.spinnerService.show();
    this.loadDashboard();
    setTimeout(() => {
      this.loadDashboard();
      this.spinnerService.hide();
    }, 5000);
  }

  loadDashboard() {
    // Get list of domains..
    this.dashboardService.getAllDomains().subscribe((data: any[]) => {
      this.domainList = data;
    })

    // Get activity report for default linkedin domain & for current date..
    this.dashboardService.getTodayReport(this.defaultDomainId, this.formatDate(this.calendar.getToday())).subscribe(data => {
      this.reportList = data;
    })

    // Get activity times for diff activities for tooltip..
    this.loadActivityTimes(this.defaultDomainId);

    // Get columns based on selected domain..
    this.loadColumns(this.defaultDomainId);
  }

  loadActivityTimes(domainId: number) {
    this.dashboardService.getActivityTime(domainId).subscribe((data: any[]) => {
      this.activityTimeList = data;
    })

  }

  loadColumns(domainId: any) {
    this.dashboardService.getDomainColumns(domainId).subscribe((data: any[]) => {
      this.columnList = data;
    })
  }

  ngOnSubmit() {
    this.spinnerService.show();
    let id = this.formControls.domain.value;
    this.loadColumns(id);
    this.loadActivityTimes(id);
    this.dashboardService.getActivityReport(id, this.formatDate(this.fromDate), this.formatDate(this.toDate)).subscribe(data => {
      this.reportList = data;
      console.log(this.reportList);
      this.spinnerService.hide();
    },
      error => console.log(error));
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  formatDate(date: any): string {
    return date ? date.year + this.DELIMITER + date.month + this.DELIMITER + date.day : '';
  }

}
