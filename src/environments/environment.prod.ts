const ORIGIN = new URL(window.location.href).origin;

export const environment = {
  production: true,
  envName: 'prod',
  activityTrackerRest: new URL('activity-tracker-rest', ORIGIN).href,
};
